package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.rev.Rev2mDistanceSensor;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Hardware;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvInternalCamera;
import org.openftc.easyopencv.OpenCvPipeline;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

@Autonomous(name="SkyStone red", group="A")
//@Disabled
public class Stone_foundation_red extends LinearOpMode {
    int counter[]= new int[3];
    double r1[]=new double[3], r2[]=new double[3], r3[]=new double[3];

    int counter_[]=new int[3];
    int n;
    private static int valMid = -1;
    private static int valLeft = -1;
    private static int valRight = -1;


    private static float rectHeight = .6f/8f;
    private static float rectWidth = 1.5f/8f;

    private static float offsetX = 1f/8f;//changing this moves the three rects and the three circles left or right, range : (-2, 2) not inclusive
    private static float offsetY = 1.5f/8f;//changing this moves the three rects and circles up or down, range: (-4, 4) not inclusive

    private static float[] midPos = {4f/8f+offsetX, 4f/8f+offsetY};//0 = col, 1 = row
    private static float[] leftPos = {2f/8f+offsetX, 4f/8f+offsetY};
    private static float[] rightPos = {6f/8f+offsetX, 4f/8f+offsetY};
    //moves all rectangles right or left by amount. units are in ratio to monitor

    private final int rows = 640;
    private final int cols = 480;

    OpenCvCamera phoneCam;

    Drivetrain drivetrain = new Drivetrain();
    Intake intake = new Intake();
    Manipulator manipulator = new Manipulator();
    ElapsedTime prog = new ElapsedTime();

    public void runOpMode() {
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        phoneCam = OpenCvCameraFactory.getInstance().createInternalCamera(OpenCvInternalCamera.CameraDirection.BACK, cameraMonitorViewId);

        phoneCam.openCameraDevice();//open camera
        phoneCam.setPipeline(new StageSwitchingPipeline());//different stages
        phoneCam.startStreaming(rows, cols, OpenCvCameraRotation.UPRIGHT);

        drivetrain.init(hardwareMap);
        drivetrain.initMover();

        manipulator.init(hardwareMap);
        intake.init(hardwareMap);

        double coef[]=drivetrain.getPIDCoeficients();
        drivetrain.SetMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        while(!isStarted())
        {
            if(gamepad1.y)
            {
                coef[0]+=0.001;
            }
            if(gamepad1.a)
            {
                coef[0]-=0.001;
            }
            if(gamepad1.x)
            {
                coef[2]-=0.00002;
            }
            if(gamepad1.b)
            {
                coef[2]+=0.00002;
            }
            if(gamepad1.left_bumper)
            {
                coef[1]-=0.5;
            }
            if(gamepad1.right_bumper)
            {
                coef[1]+=0.5;
            }
            telemetry.addData("proportional ", coef[0]);
            telemetry.addData("diferential ", coef[2]);
            telemetry.addData("integral ", coef[1]);
            telemetry.update();
            sleep(50);
            drivetrain.setPIDCoeficients(coef);
        }
        drivetrain.SetMode(DcMotor.RunMode.RUN_USING_ENCODER);
        waitForStart();

        prog.reset();

        if (valLeft == 0) {
            n = 3;
        }
        if (valMid == 0) {
            n = 2;
        }
        if (valRight == 0) {
            n = 1;
        }
        drivetrain.MovePid(0,63,0,1,0,1);
        Wait();
        drivetrain.MovePid(90,0,0,0,0,1);
        Wait();
        if(n==1)
        {
            drivetrain.MovePid(90,48,0,-1,0,1);
            Wait();
            intake.enable(true);
            drivetrain.MovePid(45,35,0,0.45,0,1);
            Wait();
            drivetrain.MovePid(45,42,0,-1,0,1);
            manipulator.attachStone();
            Wait();
            intake.disable();
            drivetrain.MovePid(90,134,0,-1,0,1);
            Wait();
        }
        if(n==2)
        {
            drivetrain.MovePid(90,46,0,-1,0,1);
            Wait();
            drivetrain.MovePid(90, 54,1,0,0,1);
            Wait();
            intake.enable(true);
            drivetrain.MovePid(90,27,0,0.45,0,1);
            Wait();
            drivetrain.MovePid(90,60,-1,0,0,1);
            manipulator.attachStone();
            //sleep(77);
            intake.enable(false);
            Wait();
            intake.disable();
            drivetrain.MovePid(90,200,0,-1,0,1);
            Wait();
        }
        if(n==3)
        {
            drivetrain.MovePid(90,14,0,-1,0,1);
            Wait();
            drivetrain.MovePid(90, 44,1,0,0,1);
            Wait();
            intake.enable(true);
            drivetrain.MovePid(90,25,0,0.45,0,1);
            Wait();
            drivetrain.MovePid(90,50,-1,0,0,1);
            manipulator.attachStone();
            Wait();
            intake.disable();
            drivetrain.MovePid(90,244,0,-1,0,1);
            Wait();
        }
        drivetrain.MovePid(180,37,0,-0.5,0,1);
        Wait();
        drivetrain.CatchFound();
        manipulator.extend();
        //drivetrain.MovePid(180,35,0,1,0,1);
        //Wait();
        drivetrain.MovePid(90,0,0,0,0,1);
        Wait();
        drivetrain.ReleaseFound();
        manipulator.releaseStone();
        manipulator.fold();
        if(prog.seconds()<=16) {
            if (n == 1) {
                drivetrain.MovePid(90, 160, 0, 1, 1, 1);
                Wait();
            }
            if (n == 2) {
                drivetrain.MovePid(90, 180, 0, 1, 0, 1);
                Wait();
            }
            if (n == 3) {
                drivetrain.MovePid(88, 200, 0, 1, 0, 1);
                Wait();
            }
            drivetrain.MovePid(90, 56, 1, 0, 0, 1);
            Wait();
            intake.enable(true);
            drivetrain.MovePid(90, 25, 0, 0.45, 0, 1);
            Wait();
            drivetrain.MovePid(90, 58, -1, 0, 0, 1);
            manipulator.attachStone();
            intake.enable(false);
            sleep(200);
            intake.disable();
            Wait();
            drivetrain.MovePid(90, 280, 0, -1, 0, 1);
            manipulator.extend();
            Wait();
            drivetrain.SetSpeed(0, 0, 0);
            manipulator.releaseStone();
        }
        else
        {
            drivetrain.CatchFound();
            drivetrain.MovePid(290,24,0,-1,0,1);
            Wait();
        }
        drivetrain.ReleaseFound();
        drivetrain.MovePid(90,95,0,1,0,1);
        manipulator.fold();
        Wait();
        drivetrain.disable();
        manipulator.disable();
        telemetry.update();
    }
    public void Wait()
    {
        while (drivetrain.isBusy()&&opModeIsActive()){
            telemetry.addData("RCG", n);
            telemetry.addData("dist", drivetrain.getDistance());
            telemetry.addData("err", drivetrain.getErr());
            telemetry.addData("integral", drivetrain.getIntegral());
            telemetry.addData("HDG", drivetrain.heading());
            telemetry.addData("TRG", drivetrain.getTargetAngle());
            telemetry.update();
        }
    }
    static class StageSwitchingPipeline extends OpenCvPipeline
    {
        Mat yCbCrChan2Mat = new Mat();
        Mat thresholdMat = new Mat();
        Mat all = new Mat();
        List<MatOfPoint> contoursList = new ArrayList<>();

        enum Stage
        {//color difference. greyscale
            detection,//includes outlines
            THRESHOLD,//b&w
            RAW_IMAGE,//displays raw view
        }

        private StageSwitchingPipeline.Stage stageToRenderToViewport = StageSwitchingPipeline.Stage.detection;
        private StageSwitchingPipeline.Stage[] stages = StageSwitchingPipeline.Stage.values();

        @Override
        public void onViewportTapped()
        {
            /*
             * Note that this method is invoked from the UI thread
             * so whatever we do here, we must do quickly.
             */

            int currentStageNum = stageToRenderToViewport.ordinal();

            int nextStageNum = currentStageNum + 1;

            if(nextStageNum >= stages.length)
            {
                nextStageNum = 0;
            }

            stageToRenderToViewport = stages[nextStageNum];
        }

        @Override
        public Mat processFrame(Mat input)
        {
            contoursList.clear();
            /*
             * This pipeline finds the contours of yellow blobs such as the Gold Mineral
             * from the Rover Ruckus game.
             */

            //color diff cb.
            //lower cb = more blue = skystone = white
            //higher cb = less blue = yellow stone = grey
            Imgproc.cvtColor(input, yCbCrChan2Mat, Imgproc.COLOR_RGB2YCrCb);//converts rgb to ycrcb
            Core.extractChannel(yCbCrChan2Mat, yCbCrChan2Mat, 2);//takes cb difference and stores

            //b&w
            Imgproc.threshold(yCbCrChan2Mat, thresholdMat, 102, 255, Imgproc.THRESH_BINARY_INV);

            //outline/contour
            Imgproc.findContours(thresholdMat, contoursList, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);
            yCbCrChan2Mat.copyTo(all);//copies mat object
            //Imgproc.drawContours(all, contoursList, -1, new Scalar(255, 0, 0), 3, 8);//draws blue contours


            //get values from frame
            double[] pixMid = thresholdMat.get((int)(input.rows()* midPos[1]), (int)(input.cols()* midPos[0]));//gets value at circle
            valMid = (int)pixMid[0];

            double[] pixLeft = thresholdMat.get((int)(input.rows()* leftPos[1]), (int)(input.cols()* leftPos[0]));//gets value at circle
            valLeft = (int)pixLeft[0];

            double[] pixRight = thresholdMat.get((int)(input.rows()* rightPos[1]), (int)(input.cols()* rightPos[0]));//gets value at circle
            valRight = (int)pixRight[0];

            //create three points
            Point pointMid = new Point((int)(input.cols()* midPos[0]), (int)(input.rows()* midPos[1]));
            Point pointLeft = new Point((int)(input.cols()* leftPos[0]), (int)(input.rows()* leftPos[1]));
            Point pointRight = new Point((int)(input.cols()* rightPos[0]), (int)(input.rows()* rightPos[1]));

            //draw circles on those points
            Imgproc.circle(all, pointMid,5, new Scalar( 255, 0, 0 ),1 );//draws circle
            Imgproc.circle(all, pointLeft,5, new Scalar( 255, 0, 0 ),1 );//draws circle
            Imgproc.circle(all, pointRight,5, new Scalar( 255, 0, 0 ),1 );//draws circle

            //draw 3 rectangles
            Imgproc.rectangle(//1-3
                    all,
                    new Point(
                            input.cols()*(leftPos[0]-rectWidth/2),
                            input.rows()*(leftPos[1]-rectHeight/2)),
                    new Point(
                            input.cols()*(leftPos[0]+rectWidth/2),
                            input.rows()*(leftPos[1]+rectHeight/2)),
                    new Scalar(0, 255, 0), 3);
            Imgproc.rectangle(//3-5
                    all,
                    new Point(
                            input.cols()*(midPos[0]-rectWidth/2),
                            input.rows()*(midPos[1]-rectHeight/2)),
                    new Point(
                            input.cols()*(midPos[0]+rectWidth/2),
                            input.rows()*(midPos[1]+rectHeight/2)),
                    new Scalar(0, 255, 0), 3);
            Imgproc.rectangle(//5-7
                    all,
                    new Point(
                            input.cols()*(rightPos[0]-rectWidth/2),
                            input.rows()*(rightPos[1]-rectHeight/2)),
                    new Point(
                            input.cols()*(rightPos[0]+rectWidth/2),
                            input.rows()*(rightPos[1]+rectHeight/2)),
                    new Scalar(0, 255, 0), 3);

            switch (stageToRenderToViewport)
            {
                case THRESHOLD:
                {
                    return thresholdMat;
                }

                case detection:
                {
                    return all;
                }

                case RAW_IMAGE:
                {
                    return input;
                }

                default:
                {
                    return input;
                }
            }
        }

    }
}
