package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

public class Manipulator {
    public boolean allowExtension=true;
    private boolean state=false;
    private DigitalChannel end1, end2;
    private CRServo slider;
    private Servo CapStone, Push;
    private ElapsedTime intend = new ElapsedTime(), extend = new ElapsedTime();
    private boolean busy;
    work work = new work();

    public void init(HardwareMap hw)
    {
        end1=hw.digitalChannel.get("en1");
        end2=hw.digitalChannel.get("en2");
        slider=hw.crservo.get("slider");
        CapStone=hw.servo.get("cap");
        Push=hw.servo.get("push");
        CapStone.setPosition(0.5);
        releaseStone();
        work.start();
    }
    public void disable()
    {
        work.interrupt();
    }

    public void attachStone(){
        Push.setPosition(0.125);
    }

    public void releaseStone()
    {
        Push.setPosition(0.65);
    }
    public void extend()
    {
        state=true;
        extend.reset();
    }

    public boolean isBusy()
    {
        return busy;
    }

    public void fold()
    {
       state=false;
       intend.reset();
    }

    public void ReleaseCap()
    {
        CapStone.setPosition(1);
    }

    class work extends Thread
    {
        public void run()
        {
            while (!isInterrupted())
            {
                if (!state && end2.getState()&&intend.seconds()<1.6&&allowExtension) {
                    busy=true;
                    slider.setPower(1);
                } else if (state && end1.getState()&&extend.seconds()<1.6) {
                    busy=true;
                    slider.setPower(-1);
                }
                else
                {
                    busy=false;
                    slider.setPower(0);
                }
            }
        }
    }
}
