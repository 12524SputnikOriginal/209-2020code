package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

import static java.lang.Thread.sleep;
@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name="new", group = "B")
public class newclass_ extends LinearOpMode
{
    DcMotor BotRight;

    double pov = 0.5;
    public void runOpMode(){
        BotRight = hardwareMap.get(DcMotor.class, "TopRight");
        waitForStart();

        BotRight.setPower(pov);
        sleep(1000);
        BotRight.setPower(0);
    }


}
