package org.firstinspires.ftc.teamcode;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;

import java.util.Set;
import java.util.function.DoubleToIntFunction;
import java.util.function.ToIntFunction;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

public class DrivetrainRTP  {
    public DcMotor Drive[]=new DcMotor[4];
    private Servo Hook1, Hook2;
    private CRServo Roulete;
    private ElapsedTime timer = new ElapsedTime();
    private BNO055IMU imu;
    public double Inddistance;
    private int start;
    Orientation angles;
    Moving mover = new Moving();
    public DistanceSensor forRange;



    private double kp=0.23/10, ki=10, kd=0.000005;
    private double SpeedX, SpeedY, TargetAngle, TargetDistance, serr=0, err, preverr, maxSpeed, u, minSpeed=0.25;
    private double FlagDistance;
    private boolean call=false, grabbed=false;
    private boolean Dcall=false;

    public boolean flag=false;

    public void init(HardwareMap hw)
    {
        Drive[0]=hw.dcMotor.get("TopRight");
        Drive[1]=hw.dcMotor.get("BotRight");
        Drive[2]=hw.dcMotor.get("BotLeft");
        Drive[3]=hw.dcMotor.get("TopLeft");
        Hook1=hw.servo.get("h1");
        Hook2=hw.servo.get("h2");
        Roulete=hw.crservo.get("park");

        Hook1.setPosition(0.28);
        Hook2.setPosition(0.72);

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit=BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit=BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imu=hw.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);
    }

    public double[] getPIDCoeficients()
    {
        double A[]=new double[3];
        A[0]=kp;
        A[1]=ki;
        A[2]=kd;
        return A;
    }

    public void setPIDCoeficients(double[] newCoeficients)
    {
        kp=newCoeficients[0];
        ki=newCoeficients[1];
        kd=newCoeficients[2];
    }

    public void disable()
    {
        SetSpeed(0,0,0);
        mover.interrupt();
    }

    public void initMover()
    {
        mover.start();
    }

    public void CatchFound()
    {
        Hook1.setPosition(0.58);
        Hook2.setPosition(0.42);
        grabbed=true;
    }

    public void ReleaseFound()
    {
        Hook1.setPosition(0.28);
        Hook2.setPosition(0.72);
        grabbed=false;
    }

    public void ReleaseRoulete()
    {
        Roulete.setPower(1);
    }

    public void FoldRoulete()
    {
        Roulete.setPower(-1);
    }

    public void StopRoulete()
    {
        Roulete.setPower(0);
    }

    public void MovePid(double targetAng, double distance, double speedX, double speedY, double flagDistance, double MaxSpeed)
    {
        TargetAngle=targetAng;
        TargetDistance=distance;
        SpeedX=speedX;
        SpeedY=-speedY;
        FlagDistance=flagDistance;
        maxSpeed=MaxSpeed;
        call=true;
    }

    public void MoveByLaser(double targetAng, double distance, double Speed)
    {
        TargetAngle=targetAng;
        TargetDistance=distance;
        SpeedY=-Speed;
        Dcall=true;
    }

    public void SetMode(DcMotor.RunMode mode)
    {
        for(int i=0; i<4; i++)
        {
            Drive[i].setMode(mode);
        }
    }

    public void SetSpeed(double x, double y,double rot)
    {
        Drive[0].setPower(-y-x+rot);
        Drive[1].setPower(x-y+rot);
        Drive[2].setPower(y+x+rot);
        Drive[3].setPower(y-x+rot);
    }

    public double heading()
    {
        double a;
        angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        a= AngleUnit.DEGREES.normalize(angles.firstAngle);
        if(a<0)
        {
            a=360+a;
        }
        return a;
    }

    public double getErr(){return err;}

    public double getIntegral(){return serr*ki;}

    public double getPower(){return u;}

    public boolean isBusy(){return call||Dcall;}

    public double getDistance()
    {
        return Distance(Drive[1].getCurrentPosition()-start);
    }

    public double getTargetAngle(){return TargetAngle;}

    private double Distance(int enc)
    {
        return 4*2.54*PI*(enc/1120.0)*2;
    }
    private int EncFromDistance(double distance)
    {
        return (int)(distance/(4*2.54*PI*2))*1120;
    }

    class Moving extends Thread {
        public void run() {
            while (!isInterrupted())
            {

                if(Dcall)
                {
                    timer.reset();
                    err=TargetAngle-heading();
                    serr=0;
                    preverr=err;
                    if(abs(heading()-TargetAngle)>2) {
                        while (!isInterrupted()) {
                            err=TargetAngle-heading();
                            if(err>180)
                            {
                                err=-360+err;
                            }
                            if(err<-180)
                            {
                                err=360+err;
                            }
                            if(abs(err)<28)
                            {
                                while(abs(err)>1) {
                                    if(!grabbed) {
                                        SetSpeed(0, 0, 0.15 * (err / abs(err)));
                                    }
                                    else
                                    {
                                        if(err/abs(err)==1) {
                                            Drive[0].setPower(0.6 * (err / abs(err)));
                                            Drive[1].setPower(0.6 * (err / abs(err)));
                                            Drive[2].setPower(0.1 * (err / abs(err)));
                                            Drive[3].setPower(0.1 * (err / abs(err)));
                                        }
                                        else
                                        {
                                            Drive[3].setPower(0.6 * (err / abs(err)));
                                            Drive[2].setPower(0.6 * (err / abs(err)));
                                            Drive[1].setPower(0.1 * (err / abs(err)));
                                            Drive[0].setPower(0.1 * (err / abs(err)));
                                        }
                                    }
                                    err=TargetAngle-heading();
                                }
                                SetSpeed(0,0,-0.163 * (err / abs(err)));
                                timer.reset();
                                while(timer.milliseconds()<21){}
                                SetSpeed(0,0,0);
                                break;
                            }
                            else
                            {
                                if(!grabbed) {
                                    SetSpeed(0,0,0.63*(err/abs(err)));
                                }
                                else
                                {
                                    if(err/abs(err)==1) {
                                        Drive[0].setPower(0.8 * (err / abs(err)));
                                        Drive[1].setPower(0.8 * (err / abs(err)));
                                    }
                                    else
                                    {
                                        Drive[2].setPower(0.8 * (err / abs(err)));
                                        Drive[3].setPower(0.8 * (err / abs(err)));
                                    }
                                }

                            }
                        }
                    }
                    double initial = forRange.getDistance(DistanceUnit.CM);
                    timer.reset();
                    err=TargetAngle-heading();
                    serr=0;
                    preverr=err;
                    while(abs(initial-forRange.getDistance(DistanceUnit.CM))<TargetDistance)
                    {
                        err=TargetAngle-heading();
                        if(err>180)
                        {
                            err=-360+err;
                        }
                        if(err<-180)
                        {
                            err=360+err;
                        }
                        if(abs(err)<25) {
                            serr = serr + err * timer.seconds();
                        }
                        if(abs(serr*ki)>0.125)
                        {
                            serr=(serr/abs(serr))*(0.125/ki);
                        }
                        u=kp*err+kp*serr+kd*(err-preverr)/timer.seconds();
                        if(abs(initial-forRange.getDistance(DistanceUnit.CM))>TargetDistance-30)
                        {
                            SpeedY=minSpeed*(SpeedY/abs(SpeedY));
                        }
                        SetSpeed(0,SpeedY,u);
                        timer.reset();
                    }
                    SetSpeed(0,-0.1,0);
                    timer.reset();
                    while (timer.milliseconds()<75){}
                    SetSpeed(0,0,0);
                    Dcall=false;
                }

                if(call)
                {
                    flag=false;
                    timer.reset();
                    err=TargetAngle-heading();
                    serr=0;
                    preverr=err;
                    if(abs(heading()-TargetAngle)>2) {
                        while (!isInterrupted()) {
                            err=TargetAngle-heading();
                            if(err>180)
                            {
                                err=-360+err;
                            }
                            if(err<-180)
                            {
                                err=360+err;
                            }
                            if(abs(err)<28)
                            {
                                while(abs(err)>1) {
                                    if(!grabbed) {
                                        SetSpeed(0, 0, 0.15 * (err / abs(err)));
                                    }
                                    else
                                    {
                                        if(err/abs(err)==1) {
                                            Drive[0].setPower(0.6 * (err / abs(err)));
                                            Drive[1].setPower(0.6 * (err / abs(err)));
                                            Drive[2].setPower(0.1 * (err / abs(err)));
                                            Drive[3].setPower(0.1 * (err / abs(err)));
                                        }
                                        else
                                        {
                                            Drive[3].setPower(0.6 * (err / abs(err)));
                                            Drive[2].setPower(0.6 * (err / abs(err)));
                                            Drive[1].setPower(0.1 * (err / abs(err)));
                                            Drive[0].setPower(0.1 * (err / abs(err)));
                                        }
                                    }
                                    err=TargetAngle-heading();
                                }
                                SetSpeed(0,0,-0.163 * (err / abs(err)));
                                timer.reset();
                                while(timer.milliseconds()<21){}
                                SetSpeed(0,0,0);
                                break;
                            }
                            else
                            {
                                if(!grabbed) {
                                    SetSpeed(0,0,0.63*(err/abs(err)));
                                }
                                else
                                {
                                    if(err/abs(err)==1) {
                                        Drive[0].setPower(0.8 * (err / abs(err)));
                                        Drive[1].setPower(0.8 * (err / abs(err)));
                                    }
                                    else
                                    {
                                        Drive[2].setPower(0.8 * (err / abs(err)));
                                        Drive[3].setPower(0.8 * (err / abs(err)));
                                    }
                                }

                            }
                        }
                    }

                    if(SpeedX!=0){TargetDistance=2*TargetDistance;}
                    SetMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    if(SpeedX==0)
                    {
                        Drive[0].setTargetPosition(EncFromDistance(TargetDistance)*(int)(SpeedY/abs(SpeedY)));
                        Drive[1].setTargetPosition(EncFromDistance(TargetDistance)*(int)(SpeedY/abs(SpeedY)));
                        Drive[2].setTargetPosition(EncFromDistance(TargetDistance)*(int)(-SpeedY/abs(SpeedY)));
                        Drive[3].setTargetPosition(EncFromDistance(TargetDistance)*(int)(-SpeedY/abs(SpeedY)));
                    }
                    else
                    {
                        Drive[0].setTargetPosition(EncFromDistance(TargetDistance)*(int)(-SpeedX/abs(SpeedX)));
                        Drive[1].setTargetPosition(EncFromDistance(TargetDistance)*(int)(SpeedX/abs(SpeedX)));
                        Drive[2].setTargetPosition(EncFromDistance(TargetDistance)*(int)(SpeedX/abs(SpeedX)));
                        Drive[3].setTargetPosition(EncFromDistance(TargetDistance)*(int)(-SpeedX/abs(SpeedX)));
                    }
                    SetMode(DcMotor.RunMode.RUN_TO_POSITION);
                    SetSpeed(SpeedX,SpeedY,0);
                    while (!isInterrupted()&&(Drive[0].isBusy()||Drive[1].isBusy()||Drive[2].isBusy()||Drive[3].isBusy()))
                    {}
                    SetSpeed(0,0,0);
                    timer.reset();
                    call=false;
                }
            }
        }
    }
}