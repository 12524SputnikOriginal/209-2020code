package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

public class Intake {

    DcMotor z1, z2;

    public void init(HardwareMap hw)
    {
        z1=hw.dcMotor.get("z1");
        z2=hw.dcMotor.get("z2");
    }
    public void enable(boolean dir)
    {
        if(!dir)
        {
            z1.setPower(0.75);
            z2.setPower(-0.75);
        }
        else {
            z1.setPower(-0.75);
            z2.setPower(0.75);
        }
    }
    public void disable()
    {
        z1.setPower(0);
        z2.setPower(0);
    }
}
