package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.util.ElapsedTime;

import static java.lang.Math.abs;

public class Lift {
    DcMotor U1, U2;
    private double kp=0.4, ki=0.145, kd=0.000000024;
    private double err1, preverr1, err2, preverr2, u1, u2;
    private double serr1=0, serr2=0;
    private double target;
    private double levelNumber, level=10, fl=6;
    private ElapsedTime timer = new ElapsedTime();
    private boolean stable;
    work Work = new work();

    public boolean allowExtention;

    private double liftPos(int enc)
    {
        double r;
        int StepsPerRotation=560;
        int D=5;
        r=D*Math.PI*enc/StepsPerRotation;
        return r;
    }


    public void init(HardwareMap hw)
    {
        U1=hw.dcMotor.get("u1");
        U2=hw.dcMotor.get("u2");
        Work.start();
    }

    public double[] getPIDCoeficients()
    {
        double A[]=new double[3];
        A[0]=kp;
        A[1]=ki;
        A[2]=kd;
        return A;
    }

    public void setPIDCoeficients(double[] newCoeficients)
    {
        kp=newCoeficients[0];
        ki=newCoeficients[1];
        kd=newCoeficients[2];
    }

    public void disable()
    {
        Work.interrupt();
    }

    public void setLevelNumber(double LevelNumber){
        levelNumber=LevelNumber;
        target=-fl-level*(LevelNumber-1);
    }

    public double getLevelNumber()
    {
        return levelNumber;
    }

    public void setTarget(double newTarget)
    {
        target=newTarget;
    }

    public double getTarget()
    {
        return target;
    }

    public boolean isStable(){
        return stable;
    }

    public double getAverageErr(){return (err1+err2)/2;}

    class work extends Thread
    {
        public void run()
        {
            int start1 = U1.getCurrentPosition();
            int start2 = U2.getCurrentPosition();
            timer.reset();
            while(!isInterrupted())
            {
                err1 = liftPos(start1-U1.getCurrentPosition())-target;
                err2 = liftPos(start2-U2.getCurrentPosition())+target;
                if(abs(err1)<3&&abs(err2)<3){
                    serr1 = serr1+err1*timer.seconds();
                    serr2=serr2+err2*timer.seconds();
                }
                if(serr1*ki>0.2)
                {
                    serr1=0.15/ki;
                }
                if(serr2*ki>0.2)
                {
                    serr2=0.15/ki;
                }
                timer.reset();
                u1=err1*kp+ki*serr1+kd*(err1-preverr1)/timer.seconds();
                u2=err2*kp+serr2*ki+kd*(err2-preverr2)/timer.seconds();
                U1.setPower(u1);
                U2.setPower(u2);
                preverr1=err1;
                preverr2=err2;
                stable=err1<1&&err2<1;
                if((err1<5&&err2<5&&target!=0)||levelNumber==0)
                {
                    allowExtention=true;
                }
                else
                {
                    allowExtention=false;
                }
            }
        }
    }
}
