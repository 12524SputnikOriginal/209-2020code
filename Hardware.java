package org.firstinspires.ftc.teamcode;


import android.graphics.Bitmap;
import android.graphics.Color;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.vuforia.Image;
import com.vuforia.PIXEL_FORMAT;
import com.vuforia.Vuforia;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Thread.sleep;
import static org.firstinspires.ftc.robotcore.external.tfod.TfodRoverRuckus.LABEL_GOLD_MINERAL;


public class Hardware {
   // public CRServo Zahvat;
    public Servo Hook1, Hook2, Push, rot, CapStone;
    public DcMotor TopLeft, BotRight, BotLeft, TopRight, z1, z2, U1, U2;
    public BNO055IMU imu;
    public CRServo slider, Park;
    //extender ex = new extender();
    DigitalChannel endstopLift, endstop1, endstop2;
    boolean rec = false;
    boolean DebugDelay_ = true;
    int delayTime = 50;
    Orientation angles;
    Acceleration gravity;
    DigitalChannel encoder1;
    DigitalChannel encoder2;
    boolean extension;
    //checker_auto keeper = new checker_auto();
    public DistanceSensor forRange;

    HardwareMap hwMap           =  null;
    private ElapsedTime period  = new ElapsedTime();

    public static double kp=0.031;
    public static double ki=1.24;
    public static double kd=-0.0000055;
    public static double serr=0;
    public static int encPerStoneY = 410;

    public void init(HardwareMap hardwareMap)
    {
        hwMap=hardwareMap;
        TopRight = hwMap.dcMotor.get("TopRight");//
        TopLeft = hwMap.dcMotor.get("TopLeft");//
        BotRight = hwMap.dcMotor.get("BotRight");//
        BotLeft = hwMap.dcMotor.get("BotLeft");//
        z1=hwMap.dcMotor.get("z1");
        z2=hwMap.dcMotor.get("z2");
        Hook1=hwMap.servo.get("h1");
        Hook2=hwMap.servo.get("h2");
        Push=hwMap.servo.get("push");
        slider=hwMap.crservo.get("slider");
        U1=hwMap.dcMotor.get("u1");
        U2=hwMap.dcMotor.get("u2");
        rot=hwMap.servo.get("rot");
        CapStone=hardwareMap.servo.get("cap");
        Park=hardwareMap.crservo.get("park");
        Push.setDirection(Servo.Direction.FORWARD);
        Push.setPosition(0.65);
        Hook1.setDirection(Servo.Direction.FORWARD);
        //Hook2.setDirection(Servo.Direction.REVERSE);
        Hook1.setPosition(0.28);
        Hook2.setPosition(0.72);
        CapStone.setPosition(0.5);
        rot.setPosition(0);
        //encoder1=hardwareMap.digitalChannel.get("enc1");
        //encoder2=hardwareMap.digitalChannel.get("enc2");

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit           = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        parameters.loggingEnabled      = true;
        parameters.loggingTag          = "IMU";
        parameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imu = hardwareMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);
        U1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        U2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        //extender ex = new extender();
        //ex.run();
    }
    public void initEndstops(HardwareMap hwMap)
    {
        endstopLift=hwMap.digitalChannel.get("enLift");
        //endstop1=hwMap.digitalChannel.get("en1");
        //endstop2=hwMap.digitalChannel.get("en2");
    }
    /*public class extender extends Thread
    {
        @Override
        public void run()
        {
            while (!isInterrupted()) {
                while (!endstop2.getState()&&extension)
                {
                    slider.setPower(1);
                }
                slider.setPower(0);
            }

        }
    }*/
    /*public void extend()
    {
        while (!endstop2.getState()&&keeper.somethingIsActive())
        {
            slider.setPower(1);
        }
        slider.setPower(0);
    }
    public void intend()
    {
        while (!endstop1.getState()&&keeper.somethingIsActive())
        {
            slider.setPower(-1);
        }
        slider.setPower(0);
    }*/
    public void activateEncs()
    {
        TopRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        BotRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        TopLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        BotLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }
    public void disableEncs()
    {
        TopRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        BotRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        TopLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        BotLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }
    public void move ( double x, double y)
    {
        TopRight.setPower(-y - x);
        BotRight.setPower(x - y);
        BotLeft.setPower(y + x);
        TopLeft.setPower(y - x);
    }
    public void move_c(double x, double y, double rot)
    {
        TopRight.setPower(-y - x+rot);
        BotRight.setPower(x - y+rot);
        BotLeft.setPower(y + x+rot);
        TopLeft.setPower(y - x+rot);
    }

    public void stopp ()
    {
        TopLeft.setPower(-TopLeft.getPower());
        BotLeft.setPower(-BotLeft.getPower());
        BotRight.setPower(-BotRight.getPower());
        TopRight.setPower(-TopRight.getPower());
        period.reset();
        while (period.milliseconds()<10)
        {}
        TopLeft.setPower(0);
        BotLeft.setPower(0);
        BotRight.setPower(0);
        TopRight.setPower(0);
    }
    public void rotate ( double speed)
    {
        TopRight.setPower(speed);
        TopLeft.setPower(speed);
        BotRight.setPower(speed);
        BotLeft.setPower(speed);
    }
    public void intakeStop()
    {
        z1.setPower(0);
        z2.setPower(0);
    }
    public void intake(boolean en)
    {
        if(en)
        {
            z1.setPower(0.75);
            z2.setPower(-0.75);
        }
        else
        {
            z1.setPower(-0.75);
            z2.setPower(0.75);
        }
    }
    public void lift(boolean en)
    {
        if(en)
        {
            U1.setPower(1);
            U2.setPower(-1);
        }
        else
        {
            U1.setPower(-0.7);
            U2.setPower(0.7);
        }
    }
    public void liftStop()
    {
         U1.setPower(0);
         U2.setPower(0);
    }
    public double heading()
    {
        double a;
        angles   = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        a= AngleUnit.DEGREES.normalize(angles.firstAngle);
        if(a<0)
        {
            a=360+a;
        }
        return a;
    }
    public void DebugDelay() throws InterruptedException{
        if (DebugDelay_) {
            sleep(delayTime);
        }
    }
}
