package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;


@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "TeleOp", group="A")
public class TeleOp extends LinearOpMode {
    Drivetrain drivetrain = new Drivetrain();
    Lift lift = new Lift();
    Manipulator manipulator = new Manipulator();
    Intake intake = new Intake();
    ElapsedTime t = new ElapsedTime();
    private boolean toFold=false;

    public void runOpMode()
    {
        drivetrain.init(hardwareMap);
        lift.init(hardwareMap);
        manipulator.init(hardwareMap);
        intake.init(hardwareMap);
        drivetrain.SetMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        folder folder = new folder();
        folder.start();
        waitForStart();
        while(opModeIsActive())
        {
            drivetrain.SetSpeed(gamepad1.left_stick_x,gamepad1.left_stick_y,gamepad1.left_trigger-gamepad1.right_trigger);

            if(gamepad1.a)
            {
                drivetrain.CatchFound();
            }
            if(gamepad1.y)
            {
                drivetrain.ReleaseFound();
            }

            if(gamepad1.dpad_down||gamepad2.left_trigger>0.3)
            {
                drivetrain.ReleaseRoulete();
            } else if(gamepad1.dpad_up)
            {
                drivetrain.FoldRoulete();
            }
            else {
                drivetrain.StopRoulete();
            }

            if(gamepad2.left_bumper)
            {
                intake.enable(true);
            } else if(gamepad2.right_bumper){
                intake.enable(false);
            }
            else
            {
                intake.disable();
            }
            if(gamepad2.dpad_up&&lift.isStable()&&!gamepad2.dpad_left&&!gamepad2.dpad_right)
            {
                lift.setLevelNumber(lift.getLevelNumber()+1);
                sleep(100);
            }
            if(gamepad2.x&&!gamepad2.dpad_left&&!gamepad2.dpad_right)
            {
                double a = lift.getTarget();
                lift.setLevelNumber(lift.getLevelNumber()-0.5);
                if(a==0) {
                    lift.setTarget(a);
                }
                sleep(100);
            }
            if(gamepad2.dpad_down)
            {
                lift.setTarget(lift.getTarget()+0.02);
            }
            if(gamepad2.dpad_left)
            {
                manipulator.extend();
            }
            if(gamepad2.dpad_right)
            {
                manipulator.fold();
            }
            if(gamepad2.right_trigger>0.25)
            {
                toFold=true;
            }
            if(gamepad2.y)
            {
                manipulator.releaseStone();
            }
            if(gamepad2.a)
            {
                manipulator.attachStone();
            }
            if(gamepad1.b&&gamepad2.b)
            {
                manipulator.ReleaseCap();
            }
            telemetry.addData("stage", lift.getLevelNumber());
            telemetry.update();
        }
        manipulator.disable();
        lift.disable();
        drivetrain.disable();
    }
    class folder extends Thread{
        public void run()
        {
            while(!isInterrupted()){
                if(toFold)
                {
                    toFold=false;
                    manipulator.releaseStone();
                    lift.setLevelNumber(lift.getLevelNumber()+1);
                    t.reset();
                    while(!lift.isStable()&&t.seconds()<0.1)
                    {

                    }
                    manipulator.fold();
                    while(manipulator.isBusy())
                    {

                    }
                    lift.setLevelNumber(lift.getLevelNumber()-1);
                    lift.setTarget(0);
                }
            }
        }

    }
}
