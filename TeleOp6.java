package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;




import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.util.ElapsedTime;

import static java.lang.Math.abs;

@TeleOp(name="TeleOp 6" ,group = " actual TeleOp")
public class TeleOp6 extends LinearOpMode {
    Hardware robot = new Hardware();
    ElapsedTime timer = new ElapsedTime();
    ElapsedTime timer2 = new ElapsedTime();
    ElapsedTime extend = new ElapsedTime();
    ElapsedTime intend = new ElapsedTime();
    ElapsedTime program = new ElapsedTime();
    sklad Sklad = new sklad();
    double lift;
    double TR, TL, BR, BL, push=0.6;
    double dt1, dt2;

    boolean st0=false, slider=true;

    double kp=0.13;
    double ki=0.125;
    double kd=0.00000035;
    double Rk=1;

    double err1, preverr1, err2, preverr2;
    double serr1=0, serr2=0;
    double level=10, fl=9.5;
    double target=0;
    float i=0;
    int start1, start2;
    double u1, u2;
    boolean en=false;
    boolean slow=false;
    boolean down=true, special=false;

    DigitalChannel end1, end2;

    public void runOpMode()
    {
        robot.init(hardwareMap);;

        end1=hardwareMap.digitalChannel.get("en1");
        end2=hardwareMap.digitalChannel.get("en2");
        start2=robot.U2.getCurrentPosition();
        err2 = liftPos(start2-robot.U2.getCurrentPosition())+target;
        preverr2=err2;
        start1=robot.U1.getCurrentPosition();
        err1=liftPos(start1-robot.U2.getCurrentPosition())-target;


        while(!isStarted())
        {
            if(gamepad1.y)
            {
                kp+=0.001;
            }
            if(gamepad1.a)
            {
                kp-=0.001;
            }
            if(gamepad1.x)
            {
                kd-=0.0000001;
            }
            if(gamepad1.b)
            {
                kd+=0.0000001;
            }
            if(gamepad1.left_bumper)
            {
                ki-=0.001;
            }
            if(gamepad1.right_bumper)
            {
                ki+=0.001;
            }
            telemetry.addData("proportional ", kp);
            telemetry.addData("diferential ", kd);
            telemetry.addData("integral ", ki);
            telemetry.update();
            sleep(50);
        }

        waitForStart();
        extend.reset();
        intend.reset();
        target = 0;
        timer.reset();
        timer2.reset();
        robot.disableEncs();
        robot.initEndstops(hardwareMap);
        Sklad.start();
        while(opModeIsActive())
        {

            TR = -gamepad1.left_stick_y - gamepad1.left_stick_x -gamepad1.right_trigger + gamepad1.left_trigger;
            BR = -gamepad1.left_stick_y + gamepad1.left_stick_x -gamepad1.right_trigger + gamepad1.left_trigger;
            BL = gamepad1.left_stick_y + gamepad1.left_stick_x -gamepad1.right_trigger + gamepad1.left_trigger;
            TL = gamepad1.left_stick_y - gamepad1.left_stick_x -gamepad1.right_trigger + gamepad1.left_trigger;

            err1 = liftPos(start1-robot.U1.getCurrentPosition())-target;
            err2 = liftPos(start2-robot.U2.getCurrentPosition())+target;
            if(abs(err1)<3&&abs(err2)<3){
                serr1 = serr1+err1*timer.seconds();
                serr2=serr2+err2*timer.seconds();
            }
            if(serr1*ki>0.2)
            {
                serr1=0.15/ki;
            }
            if(serr2*ki>0.2)
            {
                serr2=0.15/ki;
            }
            timer.reset();
            u1=err1*kp+ki*serr1+kd*(err1-preverr1)/timer.seconds();
            u2=err2*kp+serr2*ki+kd*(err2-preverr2)/timer.seconds();
            /*if(robot.endstopLift.getState())
            {
                u1=0;
                u2=0;
            }*/
            if(gamepad2.dpad_up&&abs(u1)<0.2&&i<10&&!gamepad2.dpad_left&&!gamepad2.dpad_right)
            {
                i++;
                down=false;
                sleep(100);
            }
            if(gamepad2.x&&(abs(u1)<0.2||i>=11)&&robot.endstopLift.getState()&&i>0&&!gamepad2.dpad_left&&!gamepad2.dpad_right)
            {
                i-=0.5;
                sleep(100);
            }

            if(gamepad2.right_trigger>0.5)
            {
                st0=true;
            }
            if(gamepad2.dpad_down)
            {
                target+=0.2;
                down=true;
            }
            else {
                if (i == 0) {
                    target = 0;
                } else if (i == 1 && !down) {
                    target = -fl;
                } else if (!down && i != 11) {
                    target = -fl - level * (i - 1);
                }
                if (i == 11) {
                    target = -91 - liftPos(start1);
                }
            }
            if(gamepad2.x)
            {
                robot.rot.setPosition(0.92);
            }
            if(gamepad2.left_bumper)
            {
                robot.intake(true);
            }
            else {
                if (gamepad2.right_bumper) {
                    robot.intake(false);
                }
                else {
                    robot.z1.setPower(-gamepad2.left_stick_y);
                    robot.z2.setPower(gamepad2.left_stick_y);
                }
            }
            if(gamepad1.y)
            {
                robot.Hook1.setPosition(0.28);
                robot.Hook2.setPosition(0.72);
            }
            if(gamepad1.a)
            {
                robot.Hook1.setPosition(0.58);
                robot.Hook2.setPosition(0.42);
            }
            if(gamepad2.y&&push<=1)
            {
                robot.Push.setPosition(0.65);
            }
            if(gamepad2.a&&push>=0)
            {
                robot.Push.setPosition(0.125);
            }
            if(gamepad2.dpad_left&&((err1<5&&target!=0)||i==0))
            {
                slider=false;
                extend.reset();
            }
            if(gamepad2.dpad_right&&err1<5)
            {
                slider=true;
                intend.reset();
            }
            if(gamepad2.left_trigger>0.3)
            {
                en=!en;
            }
            if(gamepad1.dpad_up||en)
            {
                robot.Park.setPower(1);
            }
            else if(gamepad1.dpad_down)
            {
                robot.Park.setPower(-1);
            }
            else
            {
                robot.Park.setPower(0);
            }
            if(gamepad1.b&&gamepad2.b)
            {
                robot.CapStone.setPosition(0);
            }
            if(gamepad1.right_stick_button&&gamepad1.right_stick_button!=special){
                slow=!slow;
            }
            if(slow)
            {
                Rk=0.3;
            }
            else {
                Rk=1;
            }
            robot.TopRight.setPower(Rk*TR);
            robot.BotLeft.setPower(Rk*BL);
            robot.BotRight.setPower(Rk*BR);
            robot.TopLeft.setPower(Rk*TL);
            if(u2>0)
            {
                u1=u1*0.55;
                u2=u2*0.55;
            }
            if(robot.endstopLift.getState()||u2<0) {
                robot.U2.setPower(u2);
                robot.U1.setPower(u1);
            }
            else
            {
                robot.U1.setPower(0);
                robot.U2.setPower(0);
            }
            if (slider && end2.getState()&&intend.seconds()<1.6) {
                robot.slider.setPower(1);
            } else if (!slider && end1.getState()&&extend.seconds()<1.6) {
                robot.slider.setPower(-1);
            }
            else
            {
                robot.slider.setPower(0);
            }
            special=gamepad1.right_stick_button;
            telemetry.addData("Lift_pos", liftPos(robot.U2.getCurrentPosition()-start2));
            telemetry.addData("lift_zero_pos", liftPos(start2));
            telemetry.addData("target",liftPos(start2)+target);
            telemetry.addData("err1", err1);
            telemetry.addData("u2", u2);
            telemetry.addData("Stage ", i+1);
            telemetry.addData("h1", robot.Hook1.getPosition());
            telemetry.addData("h2", robot.Hook2.getPosition());
            telemetry.addData("Stvor ", push);
            telemetry.update();
            preverr1=err1;
            preverr2=err2;
        }
    }
    public double liftPos(int enc)
    {
        double r;
        int StepsPerRotation=560;
        int D=5;
        r=D*Math.PI*enc/StepsPerRotation;
        return r;
    }
    class sklad extends Thread
    {
        public void run()
        {
            while(!isInterrupted()&&opModeIsActive()) {
                if(st0) {
                    target=target-level;
                    robot.Push.setPosition(0.65);
                    down=true;
                    while (err1>1)
                    {

                    }
                    timer2.reset();
                    program.reset();
                    while (program.seconds()<0.37){}
                    slider = true;
                    intend.reset();
                    while (end2.getState()&&intend.seconds()<2) {
                        robot.slider.setPower(1);
                    }
                    robot.slider.setPower(0);
                    target = 0;
                    st0=false;
                }
            }

        }
    }
    //капстоун 0.5-держим 1- сброс
}