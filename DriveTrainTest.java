package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

@Autonomous(name="DT test", group="B")
public class DriveTrainTest extends LinearOpMode {
    Drivetrain drivetrain = new Drivetrain();
    ElapsedTime timer = new ElapsedTime();
    DcMotor TopRight, BotRight, BotLeft, TopLeft;
    private double kp=0.222/10, ki=2.5*10, kd=0.0012;
    double err, preverr, serr=0;
    boolean flag;
    double u, maxSpeed;
    double minSpeed=0.3;

    public void runOpMode()
    {
        drivetrain.init(hardwareMap);
        double coef[]=drivetrain.getPIDCoeficients();

        while(!isStarted())
        {
            TopRight=hardwareMap.dcMotor.get("TopRight");
            BotRight=hardwareMap.dcMotor.get("BotRight");
            BotLeft=hardwareMap.dcMotor.get("BotLeft");
            TopLeft=hardwareMap.dcMotor.get("TopLeft");
            if(gamepad1.y)
            {
                kp+=0.001;
            }
            if(gamepad1.a)
            {
                kp-=0.001;
            }
            if(gamepad1.x)
            {
                kd-=0.00002;
            }
            if(gamepad1.b)
            {
                kd+=0.00002;
            }
            if(gamepad1.left_bumper)
            {
                ki-=0.1;
            }
            if(gamepad1.right_bumper)
            {
                ki+=0.1;
            }
            telemetry.addData("proportional ", coef[0]);
            telemetry.addData("diferential ", coef[2]);
            telemetry.addData("integral ", coef[1]);
            telemetry.update();
            sleep(50);
            drivetrain.setPIDCoeficients(coef);
        }

        waitForStart();

        movePid(0, 0, 1, 180, 0);
    }
    private double Distance(int enc)
    {
        return 4*2.54*3.1415*(enc/1120.0)*2-2;
    }
    public void movePid(double ang, double speedY, double speedX, double distance, double flagg)
    {
        TopRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        BotRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        BotLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        TopLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        speedY= -speedY;
        flag=false;
        timer.reset();
        err=ang-drivetrain.heading();
        serr=0;
        preverr=err;
        while (opModeIsActive())
        {
            preverr=err;
            if(abs(err)<12) {
                serr = serr + err * timer.seconds();
            }
            u=kp*err+ki*serr+kd*(err-preverr)/timer.seconds();


            drivetrain.SetSpeed(0,0,u);
            err=ang-drivetrain.heading();
            if(err>180)
            {
                err=-360+err;
            }
            if(err<(-180))
            {
                err=360+err;
            }
            if(abs(err)<0.85&&abs(err-preverr)/timer.seconds()<0.1)
            {
                break;
            }
            timer.reset();
        }
        int start = TopRight.getCurrentPosition();
        if(speedX!=0){distance=2*distance;}
        while (opModeIsActive())
        {
            err=ang-drivetrain.heading();
            if(err>180)
            {
                err=-360+err;
            }
            if(err<-180)
            {
                err=360+err;
            }
            if(abs(err)<25) {
                serr = serr + err * timer.seconds();
                if(serr*ki>0.125)
                {
                    serr=0.125/ki;
                }
            }
            u=kp*err+kp*serr+kd*(err-preverr)/timer.seconds();
            if(distance-abs(Distance(TopRight.getCurrentPosition()-start))<15&&speedX==0)
            {
                speedY=minSpeed*(speedY/abs(speedY));
            }
            if(distance-abs(Distance(TopRight.getCurrentPosition()-start))<22&&speedX!=0)
            {
                speedX=minSpeed*(speedX/abs(speedX));
            }
            if(abs(Distance(TopRight.getCurrentPosition()-start))>=distance)
            {
                break;
            }
            drivetrain.SetSpeed(speedX,speedY,u);
            telemetry.addData("distance",abs(Distance(TopRight.getCurrentPosition()-start)));
            telemetry.update();
        }
        drivetrain.SetSpeed(0,0,0);
    }
}
