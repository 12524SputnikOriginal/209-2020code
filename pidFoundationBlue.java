package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import static java.lang.Math.abs;

@Autonomous(name="Foundation Blue by PID", group="actual autonomous")
@Disabled
public class pidFoundationBlue extends LinearOpMode {
    Hardware robot = new Hardware();
    ElapsedTime timer = new ElapsedTime();
    ElapsedTime program = new ElapsedTime();
    public void runOpMode()
    {
        robot.init(hardwareMap);
        waitForStart();

        program.reset();

        movePid(0,-0.4,100);
        movePid(0.8, 0, 900);
        autoMove(0,0.8,1000);
        autoMove(0,0.4, 282);

        robot.Hook1.setPosition(0.46);
        robot.Hook2.setPosition(0.57);
        sleep(500);
        autoMove(0,-0.8, 1370);
        sleep(500);
        rotateTo(90, 1);

        robot.Hook1.setPosition(0.71);
        robot.Hook2.setPosition(0.318);

        autoMove(0.5, 0, 380);
        autoMove(0, 0.8, 450);
        autoMove(0, -0.8, 200);

        autoMove(0.7,0, 2000);
        sleep(2000);
        robot.stopp();
        movePid(-0.5, 0, 260);
        movePid(0,-0.9, 1750);
    }
    public void movePid(double x, double y, int enc)
    {
        double target = robot.heading();
        robot.activateEncs();
        int enc0 = robot.TopRight.getCurrentPosition();
        double err, preverr,p;
        err = target - robot.heading();
        preverr=err;
        int remain;
        while(abs(robot.TopRight.getCurrentPosition()-enc0)<enc && opModeIsActive()) {
            err = target - robot.heading();
            if (err > 180) {
                err = -360 + err;
            }
            if (err < -180) {
                err = 360 + err;
            }
            if (abs(err) < 8) {
                robot.serr = robot.serr + err * 0.001;
            }
            p = robot.kp * err + robot.ki * robot.serr + robot.kd * (err - preverr) / 0.001;
            robot.move_c(x, y, p);
            preverr=err;
            sleep(1);
            telemetry.addData("angle ", robot.heading());
            telemetry.addData("err ",err);
            telemetry.addData("power ",p);
            telemetry.addData("defirential ", robot.kd*(err-preverr)/0.001);
            telemetry.update();
        }
        robot.stopp();
        sleep(100);
        rotateTo(target, 0.8);
        robot.disableEncs();
    }
    public void rotateTo(double target, double pmax){

        timer.reset();
        double p=1;
        double err = target-robot.heading();
        double preverr = err;
        while(opModeIsActive())
        {
            err=target-robot.heading();
            if(err>180)
            {
                err=-360+err;
            }
            if(err<-180)
            {
                err=360+err;
            }
            if(abs(err)<8) {
                robot.serr = robot.serr + err * 0.001;
            }
            p=robot.kp*err+robot.ki*robot.serr+robot.kd*(err-preverr)/0.001;
            if(p>pmax)
            {
                p=pmax;
            }
            robot.rotate(p);
            timer.reset();
            telemetry.addData("angle ", robot.heading());
            telemetry.addData("err ",err);
            telemetry.addData("power ",p);
            telemetry.addData("defirential ", robot.kd*(err-preverr)/0.001);
            telemetry.update();
            if(abs((err-preverr)/0.001)<0.001&&abs(err)<0.85)
            {
                break;
            }
            preverr=err;
            sleep(1);
        }
        robot.stopp();
        sleep(100);
    }
    public void autoMove(double x, double y, int enc)
    {
        int pos;

        robot.activateEncs();
        pos=robot.TopRight.getCurrentPosition();
        while(abs(-pos+robot.TopRight.getCurrentPosition())<enc&&opModeIsActive())
        {
            robot.TopRight.setPower(-y - x);
            robot.BotRight.setPower(x - y);
            robot.BotLeft.setPower(y + x);
            robot.TopLeft.setPower(y - x);
        }
        robot.stopp();
        robot.disableEncs();
    }
}
